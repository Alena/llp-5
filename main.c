#include "bmp_utils.h"
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char* argv[]){

    FILE* file = fopen(argv[1], "rb");
    int angle = 90;

    if (file == NULL){
        printf("Couldn't open file\n");
        return 0;
    }else{

        struct image_t* bmp_map = malloc(sizeof(struct image_t));
        enum read_status rs = read_bmp(file, bmp_map);

        if (rs == READ_OK){
            show(bmp_map);

            printf("\nEnter angle: ");
            scanf("%d",&angle);
            bmp_map = rotate_any_angle(bmp_map, angle);

            show(bmp_map);

            fclose(file);
            file = fopen(argv[1], "wb");
            enum write_status ws = write_bmp(bmp_map, file, 0x4D42);

            if(ws == WRITE_OK)
                printf("New bmp image_t successfully save\n");
            else
                printf("Couldn't save bmp image_t\n");

        }else{
            printf("Couldn't read file\n");
        }
        free(bmp_map);
    }

    fclose(file);
    return 0;
}
