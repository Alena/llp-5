//
// Created by Alena on 09.12.2020.
//

#ifndef LLP_6_BMP_UTILS_H
#define LLP_6_BMP_UTILS_H
#include <stdio.h>
#include <stdint.h>
#include "status.h"
#include "image_struct.h"
#include "bmp_struct.h"

enum read_status read_bmp(FILE* file, struct image_t*  img);

struct image_t* rotate_any_angle(struct image_t*  in, int angle);

enum write_status write_bmp(struct image_t* img, FILE* file, uint16_t format);

void show(struct image_t* img);

#endif //LLP_6_BMP_UTILS_H
