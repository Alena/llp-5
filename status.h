//
// Created by Alena on 09.12.2020.
//

#ifndef LLP_6_STATUS_H
#define LLP_6_STATUS_H

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum write_status {
    WRITE_OK = 0,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_BIT_MAP
};

#endif //LLP_6_STATUS_H
