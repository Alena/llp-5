//
// Created by Alena on 09.12.2020.
//

#ifndef LLP_6_BMP_STRUCT_H
#define LLP_6_BMP_STRUCT_H

#include <stdint.h>

#define bfReserved_DEFAULT 0
#define bOffBits_DEFAULT 54
#define biSize_DEFAULT 40
#define biPlanes_DEFAULT 1
#define biBitCount_DEFAULT 24
#define biCompression_DEFAULT 0
#define biSizeImage_DEFAULT 0
#define biXPelsPerMeter_DEFAULT 0
#define biYPelsPerMeter_DEFAULT 0
#define biClrUsed_DEFAULT 0
#define biClrImportant_DEFAULT 0


struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

#endif //LLP_6_BMP_STRUCT_H
