//
// Created by Alena on 09.12.2020.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "status.h"
#include "image_struct.h"
#include "bmp_struct.h"


enum read_status read_bmp(FILE* file, struct image_t* const img) {
    struct bmp_header* header = malloc(sizeof(struct bmp_header));

    if (fread(header, sizeof(struct bmp_header), 1, file) != 1)
        return READ_INVALID_SIGNATURE;

    fseek(file, header->bOffBits, SEEK_SET);

    if (header->bfType != 0x4D42)
        return READ_INVALID_HEADER;

    if (header->biBitCount != 24)
        return READ_INVALID_HEADER;

    uint32_t width = header->biWidth;
    uint32_t height = header->biHeight;
    uint32_t size_row = width * sizeof(struct pixel_t);
    char empty[4];
    uint32_t empty_bits = size_row % 4 == 0 ? 0 : 4 - size_row % 4;

    struct pixel_t* bitMap = malloc(height * width * sizeof(struct pixel_t));

    for (size_t i = 0; i < height; i ++){
        if (fread(bitMap + i*width, size_row, 1, file) != 1)
            return READ_INVALID_BITS;
        if (empty_bits != 0){
            fread(empty, empty_bits, 1, file);
        }
    }

    img->data = bitMap;
    img->width = header->biWidth;
    img->height = header->biHeight;
    free(header);
    return READ_OK;

}


enum write_status write_bmp(struct image_t* const img, FILE* file, uint16_t format) {
    struct bmp_header* header = malloc(sizeof(struct bmp_header));
    struct pixel_t* bitMap = img->data;
    uint32_t height = img->height;
    uint32_t width = img->width;
    char empty[4] = {0};
    uint32_t empty_bits = width * sizeof(struct pixel_t) % 4 == 0 ? 0 : 4 - width * sizeof(struct pixel_t) % 4;

    header->bfType = format;
    header->bfileSize = sizeof(struct bmp_header) +  height * (width*sizeof(struct pixel_t) + empty_bits);
    header->bfReserved = bfReserved_DEFAULT;
    header->bOffBits = bOffBits_DEFAULT;
    header->biSize = biSize_DEFAULT;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = biPlanes_DEFAULT;
    header->biBitCount = biBitCount_DEFAULT;
    header->biCompression = biCompression_DEFAULT;
    header->biSizeImage = height * (width*sizeof(struct pixel_t) + empty_bits);
    header->biXPelsPerMeter = biXPelsPerMeter_DEFAULT;
    header->biYPelsPerMeter = biYPelsPerMeter_DEFAULT;
    header->biClrUsed = biClrUsed_DEFAULT;
    header->biClrImportant = biClrImportant_DEFAULT;

    if (fwrite(header, sizeof(struct bmp_header), 1, file) != 1)
        return WRITE_INVALID_HEADER;

    for (size_t i = 0; i < height; i ++) {
        if (fwrite((bitMap + i*width), sizeof(struct pixel_t) , width, file) != width)
            return WRITE_INVALID_BIT_MAP;

        if (empty_bits != 0)
            fwrite(&empty, sizeof(char), empty_bits, file);
    }

    free(header);
    free(img->data);
    return WRITE_OK;
}


void show(struct image_t* img) {
    struct pixel_t* bitMap = img->data;
    printf("BMP image_t RGB\n");
    for (size_t i = 0; i < img->height; i ++){
        for (size_t j = 0; j < img->width; j ++)
            printf("| %d | %d | %d |\n", (*(bitMap + i * img->width + j)).r, (*(bitMap + i * img->width + j)).g, (*(bitMap + i * img->width + j)).b);
        printf("\n");
    }
}


static void rotate_vector(double *new_x, double *new_y, double prev_x, double prev_y, double rad){
    *new_x = prev_x * cos(rad) - prev_y * sin(rad);
    *new_y = prev_x * sin(rad) + prev_y * cos(rad);

}


static double dmax4(double const v1, double const v2, double const v3, double const v4) {
    double max = v1;
    if (v2 > max)
        max = v2;
    if (v3 > max)
        max = v3;
    if (v4 > max)
        max = v4;
    return max;
}


static void calc_new_size(struct image_t* const result, struct image_t* const source, double rad){
    double right = source->width / 2.0;
    double left = -right;
    double top = source->height / 2.0;
    double bottom = -top;

    double lt_x, lt_y, rt_x, rt_y, lb_x, lb_y, rb_x, rb_y;
    rotate_vector(&lt_x, &lt_y, left, top, rad);
    rotate_vector(&rt_x, &rt_y, right, top, rad);
    rotate_vector(&lb_x, &lb_y, left, bottom, rad);
    rotate_vector(&rb_x, &rb_y, right, bottom, rad);

    result->width = (uint32_t) round(dmax4(lt_x, rt_x, lb_x, rb_x) * 2 ) ;
    result->height = (uint32_t) round(dmax4(lt_y, rt_y, lb_y, rb_y) * 2 ) ;
}


struct image_t* rotate_any_angle(struct image_t* const img, int64_t angle){
    double rad = angle * M_PI / 180;
    double pivot_x = (double )(img->width - 1) / 2.f , pivot_y = (double)(img->height - 1) / 2.f;
    struct image_t* new_bmp = malloc(sizeof(struct image_t));
    calc_new_size(new_bmp, img, rad);
    new_bmp->data = malloc(new_bmp->width * new_bmp->height * sizeof(struct pixel_t));
    double new_pivot_x = (double)(new_bmp->width - 1) / 2.f;
    double new_pivot_y = (double)(new_bmp->height - 1) / 2.f;

    for(uint32_t y = 0; y < new_bmp -> height; y++){
        for (uint32_t x = 0; x < new_bmp -> width; x++) {
            double old_x, old_y;
            double  index_old_x, index_old_y;
            rotate_vector(&old_x, &old_y, x-new_pivot_x, y-new_pivot_y, -rad);
            index_old_x = round(old_x + pivot_x);
            index_old_y = round(old_y + pivot_y);
            if(index_old_x < 0 || index_old_x > img->width - 1 || index_old_y < 0 || index_old_y > img->height - 1) {
                new_bmp->data[y * new_bmp->width + x] = (struct pixel_t) {255, 255, 254};
            }
            else
                new_bmp->data[y * new_bmp->width + x] = img->data[(uint32_t)index_old_y * img->width + (uint32_t)index_old_x];
        }
    }

    return new_bmp;
}

